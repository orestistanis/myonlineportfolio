$(document).ready(function () {

    $("span:nth-child(1)").css("color", "orange");
    
    // Select all span elements which are children of p's
    $("p > span").css("color", "rgb(0, 204, 255)");
    // Select all span elements which are children of element with id=copyright-text
    $('span', $('#copyright-text')).css("color", "rgb(0, 204, 255)");
    $(".orange").css("color", "orange");
    $(".cyan").css("color","rgb(0, 204, 255)");
    $(".green").css("color","#1ebba3");

    // Pause the video of the modal when the users closes the modal
    $('#spaceTimeModal').on('hidden.bs.modal', function() {
        var htmlVideo = document.getElementById("htmlVideo");
        if (htmlVideo != null) {
          htmlVideo.pause();
          htmlVideo.currentTime = 0;
        }
      });

    // When the user scrolls down 80px from the top of the document, resize the navbar's padding
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
            $("#navbar").css( "padding", "10px 40px" );
        } else {
            $("#navbar").css( "padding", "30px 40px" );
        }

    }
   
    
});


